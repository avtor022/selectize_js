This Slectize_JS JS based script. It's usefull for searching and selecting at the same time.


To initialize selectize_js for units data in JS script add 

    inputSelectizing(unit_data_text_field_id, unit_data, show_keys: true/false)

Format of data for selectize_js is 

    {unit1.id: unit1.value}, {unit2.id: unit2.value},...]
