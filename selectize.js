// to initialize custom_selectizing you need to set
// inputSelectizing(inputId, dataToSelect, showKeys = false)
// in $(document).on('turbolinks:load', function(event)

// function for add event listeners to input marked like inputSelectizing(inputId, dataToSelect, showKeys = false)
function inputSelectizing(inputId, dataToSelect, showKeys = false) {
  let inputElement = document.getElementById(inputId);
  if (inputElement) {
    inputElement.autocomplete = 'off'
    inputElement.addEventListener('focus', () => {
      let dataLists = document.querySelectorAll('.data-select-list');
      if (dataLists.length != 0) {
        dataLists.forEach((dataList) => dataList.remove())
      }
      else {
        inputElement.select();
        customSelectUpdating(inputId, dataToSelect, inputElement.value, inputElement.id, showKeys)
      }
    })
    inputElement.addEventListener('input', () =>
      customSelectUpdating(inputId, dataToSelect, inputElement.value, inputElement.id, showKeys), true);
    inputElement.addEventListener('keydown', (event) => {
      if (event.code == 'ArrowDown') {
        let dataStr = document.getElementsByClassName('data-select-str')[0];
        if (dataStr) {
          dataStr.focus()
        }
      } else if (event.code == 'Escape') {
        let dataList = document.getElementById(`${inputElement.id}_list`);
        if (dataList) {
          dataList.remove()
        }
      }
    }, true)
  }
}

// function for create div with values strings
function customSelectAdding(inputId, dataToSelect, inputTitle, showKeys = false) {
  // create div after selectize input
  let inputElement = document.getElementById(inputId),
      dataList = document.createElement('div');
  dataList.id = `${inputTitle}_list`;
  dataList.className = `${inputTitle}-list data-select-list`
  // row creating for each value in data from db
  dataToSelect.forEach( (item) => {
    let dataStr = document.createElement('div');
    dataStr.id = `dataStr_${item[0]}`;
    dataStr.tabIndex = 0;
    dataStr.className = `${inputTitle}-str data-select-str`;
    dataStr.value = item[0];
      if (showKeys) {
        dataStr.innerHTML = `${item[1]} (${item[0]})`
      }
      else {
        dataStr.innerHTML = item[1]
      }
      // event listeners adding to each data from db row
      'mouseover focus'.split(' ').forEach((e) => {
        dataStr.addEventListener( e, () => {
          document.querySelectorAll('.active').forEach((activeStr) => activeStr.classList.remove('active'))
          dataStr.classList.add('active')
          dataStr.focus()
        })
      });
      'mouseout blur'.split(' ').forEach((e) => {
        dataStr.addEventListener( e, () => {
          document.querySelectorAll('.active').forEach((activeStr) => activeStr.classList.remove('active'))
        })
      });
      'keydown click'.split(' ').forEach((e) => {
        dataStr.addEventListener( e, (event) => {
          if ((e == 'keydown' && (event.code == 'Enter' || event.code == 'NumpadEnter')) || e == 'click') {
            gettingDataFromSelect (inputId, inputTitle, dataToSelect, dataStr.value)
          } else if (e == 'keydown' && event.code == 'ArrowDown') {
            let evt = new KeyboardEvent('keydown', {'keyCode': 9});
            document.dispatchEvent(evt);
          }
        })
      })
      // dataStr.addEventListener('click', () =>
      //   gettingDataFromSelect (inputId, inputTitle, dataToSelect, dataStr.value));
      dataList.appendChild(dataStr);
    })
  inputElement.after(dataList);
  // add 'active' class to the first row in values list
  let activeStr = dataList.getElementsByClassName(`${inputTitle}-str`)[0];
  if (activeStr) {
    activeStr.classList.add('active')
  }
}

// function of data converting from object to array of arrays
function dataToSelectSorting(obj, query) {
  let sortable=[];
  for(var key in obj)
    if(obj.hasOwnProperty(key))
      sortable.push([key, obj[key]]);
  return sortAscending(sortable)
}

function sortAscending (sortable) {
  return sortable.sort(function(a, b) {
    var x=a[1].toLowerCase(),
        y=b[1].toLowerCase();
    return x<y ? -1 : x>y ? 1 : 0;
  });
}

// function of values list removing
function customSelectRemoving (inputTitle) {
  document.getElementById(`${inputTitle}_list`).remove()
}

// function of data filter by value in input
function filterItems(dataForFilter, query) {
  let dataForFilterKeys = new Array(),
      dataForFilterValues = new Array();
  for (let key in dataForFilter) {
    dataForFilterKeys.push(key);
    dataForFilterValues.push(dataForFilter[key])
  }
  dataForFilterValues = dataForFilterValues.filter(function(el) {
    return el.toLowerCase().indexOf(query.toLowerCase()) > -1;
  });
  dataForFilterKeys = dataForFilterKeys.filter(function(el) {
    return el.toLowerCase().indexOf(query.toLowerCase()) > -1;
  });
  return [dataForFilterKeys, dataForFilterValues]
}

// function for values in list updating
// if showKeys == true then output is filtered data by values + data filtered by keys
function customSelectUpdating (inputId, dataForFilter, query, inputTitle, showKeys = false) {
  document.querySelectorAll('.data-select-list').forEach(function(element){
    element.remove()
  })
  let arrayFilterKeys = filterItems(dataForFilter, query)[0],
      arrayFilterValues = filterItems(dataForFilter, query)[1],
      dataToSelectFiltered = new Object();
  document.getElementById(inputId).data = '';
  for (let key in dataForFilter) {
    if (showKeys) {
      if (arrayFilterValues.indexOf(dataForFilter[key]) != -1 || arrayFilterKeys.indexOf(key) != -1) {
        dataToSelectFiltered[key] = dataForFilter[key]
      }
    }
    else if (arrayFilterValues.indexOf(dataForFilter[key]) != -1) {
      dataToSelectFiltered[key] = dataForFilter[key]
    }
  }
  let dataToSelect = dataToSelectSorting(dataToSelectFiltered, query),
      selectList = document.getElementById(`${inputTitle}_list`);
  if (selectList) {
    selectList.remove();
  }
  customSelectAdding(inputId, dataToSelect, inputTitle, showKeys)
}

// function of data from select putting into input element
function gettingDataFromSelect (inputId, inputTitle, dataToSelect, key) {
  let inputElement = document.getElementById(inputId);
  inputElement.value = dataToSelect.find(item => item[0] == key)[1];
  inputElement.data = key;
  inputElement.focus();
}
